require 'rails_helper'

RSpec.describe "application.helper_spec.rb", type: :helper do
  include ApplicationHelper

  describe "full_title" do
    subject { full_title(page_title: title) }
    context "when page_title is nil" do
      let(:title) { nil }
      it { is_expected.to eq 'BIGBAG Store' }
    end

    context "when page_title is empty" do
      let(:title) { '' }
      it { is_expected.to eq 'BIGBAG Store' }
    end

    context "when page_title is exist" do
      let(:title) { 'title' }
      it { is_expected.to eq "title - BIGBAG Store" }
    end
  end
end
