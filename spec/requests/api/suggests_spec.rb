require 'rails_helper'

RSpec.describe "Potepan::Api::V1::Suggests", type: :request do
  describe "GET potepan/api/v1/suggests" do
    let!(:header) { { Authorization: "Bearer #{api_key}" } }
    let!(:api_key) { Rails.application.credentials.suggest_api_key }

    before { ["rails", "ruby", "ruby on rails", "ruby for men", "ruby for women", "rails for animal"].each { |name| create(:suggest, keyword: name) } }

    describe "keyword" do
      context "when keyword =''" do
        it "returns status 400" do
          get potepan_api_v1_suggests_path, headers: header, params: { keyword: '' }
          expect(response.body).to eq "keyword can't be blank"
          expect(response).to have_http_status 400
        end
      end

      context "when keyword = r" do
        it "returns correct suggests" do
          get potepan_api_v1_suggests_path, headers: header, params: { keyword: 'r'}
          expect(JSON.parse(response.body)).to eq ["rails", "ruby", "ruby on rails", "ruby for men", "ruby for women", "rails for animal"]
        end
      end

      context "when keyword isn't exist in query" do
        it "returns status 400" do
          get potepan_api_v1_suggests_path, headers: header
          expect(response.body).to eq "keyword can't be blank"
          expect(response).to have_http_status 400
        end
      end
    end

    describe "max_num" do
      before do
        stub_const("Potepan::Api::V1::SuggestsController::DEFAULT_SUGGEST_COUNT", 3)
        stub_const("Potepan::Api::V1::SuggestsController::MAX_SUGGEST_COUNT", 5)
        stub_const("Potepan::Api::V1::SuggestsController::MAX_SUGGEST_RANGE", 5)
        get potepan_api_v1_suggests_path, headers: header, params: { keyword: 'r', max_num: max_num }
      end

      subject { JSON.parse(response.body).size }
      context "when max_num < 1" do
        let(:max_num) { 0 }
        it "gets the number of default" do
          is_expected.to eq 3
        end
      end

      context "when 1 <= max_num <= 5" do
        let(:max_num) { 4 }
        it "gets the number of max_num" do
          is_expected.to eq 4
        end
      end

      context "when max_num > 5" do
        let(:max_num) { 10 }
        it "gets the number of max" do
          is_expected.to eq 5
        end
      end
    end

    context "when authenticate token mismatch" do
      it "returns 401 unauthorized" do
        get potepan_api_v1_suggests_path, headers: { Authorization: "Bearer api654" }
        expect(response).to have_http_status 401
      end
    end

    describe "BadRequest occurred" do
      it "returns error message" do
        allow(Potepan::Suggest).to receive(:search_keyword).and_raise(ActionController::BadRequest)
        get potepan_api_v1_suggests_path, headers: header, params: { keyword: 'r'}
        expect(response.body).to eq "ActionController::BadRequest"
      end
    end
  end
end
