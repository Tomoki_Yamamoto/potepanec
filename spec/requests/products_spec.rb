require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "GET potepan/products/:id" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "gets related products" do
      expect(response.body).to include related_products.first.name
    end
  end
end
