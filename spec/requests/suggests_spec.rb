require 'rails_helper'
require 'webmock'

RSpec.describe "Suggests", type: :request do
  describe "GET potepan/suggest" do
    let(:api_key) { Rails.application.credentials.potepan_api_key }
    let(:url) { Rails.application.credentials.potepan_url }
    let(:word_list) { ['ruby', 'ruby for men', 'ruby for women', 'rails', 'ruby on rails'].to_json }

    before do
      WebMock.stub_request(:get, url).
        with(
          headers: { Authorization: "Bearer #{api_key}" },
          query: { keyword: keyword, max_num: 5 }
        ).
        to_return(
          body: word_list.to_json,
          status: status
        )
      get potepan_suggest_path, params: { keyword: keyword }
    end

    context "when keyword matches" do
      let(:keyword) { "r" }
      let(:status) { 200 }

      it "returns word_list" do
        expect(JSON.parse(response.body)).to eq ['ruby', 'ruby for men', 'ruby for women', 'rails', 'ruby on rails']
      end
    end

    context "when keyword doesn't match" do
      let(:keyword) { " " }
      let(:status) { 400 }

      it "returns 400 bad request" do
        expect(response).to have_http_status 400
      end
    end
  end
end
