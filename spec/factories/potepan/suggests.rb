FactoryBot.define do
  factory :suggest, class: 'Potepan::Suggest' do
    keyword { "rails" }
  end
end
