require 'rails_helper'

RSpec.describe "Potepan::ProductDecorator", type: :model do
  describe "related_products" do
    let!(:parent_taxon) { create(:taxon) }
    let!(:child_taxon) { create(:taxon, parent: parent_taxon) }
    let!(:other_taxon) { create(:taxon) }
    let!(:product_1) { create(:product, taxons: [parent_taxon, child_taxon]) }
    let!(:product_2) { create(:product, taxons: [parent_taxon, child_taxon]) }
    let!(:other_product) { create(:product, taxons: [other_taxon])}

    it "doesn't get other_product that have other_taxon" do
      expect(product_1.related_products).not_to include other_product
    end

    it "includes product_2" do
      expect(product_1.related_products).to include product_2
    end

    it "doesn't include product_1 itself" do
      expect(product_1.related_products).not_to include product_1
    end
  end
end
