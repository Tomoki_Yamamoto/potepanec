require 'rails_helper'

RSpec.describe "categories/show.html.erb", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, parent_id: taxonomy.root.id) }
  let(:leaves_taxon) { create(:taxon, parent: taxon) }
  let!(:product_1) { create(:product, taxons: [taxon]) }
  let!(:product_2) { create(:product, taxons: [taxon]) }
  let!(:product_3) { create(:product, taxons: [leaves_taxon]) }
  let(:image) { create(:image) }

  before do
    product_1.images << image
    visit potepan_category_path(taxon.id)
  end

  describe "category-page" do
    context "when taxon page" do
      it "display products" do
        expect(page.all('div.productBox').size).to eq(3)
      end
    end

    context "when leaves_taxon page" do
      before do
        visit potepan_category_path(leaves_taxon.id)
      end

      it "doesn't display products are not associated to leaves_taxon" do
        expect(page).not_to have_content product_1.name
        expect(page).not_to have_content product_2.name
      end

      it "display product" do
        expect(page.all('div.productBox').size).to eq(1)
      end
    end

    it "should have the title 'taxon.name - BIGBAG Store'" do
      expect(page).to have_title("#{taxon.name} - BIGBAG Store")
    end

    it "display category name" do
      expect(page).to have_selector 'h2' ,text: taxon.name
    end

    describe "accordion menu" do
      before do
        within ".side-nav" do
          click_on taxonomy.name, match: :first
          click_on taxon.name, match: :first
        end
      end

      it "display taxonomy name" do
        expect(page).to have_content taxonomy.name
      end

      it "display taxon name" do
        expect(page).to have_content taxon.name
      end

      it "taxon link should be correct" do
        expect(current_path).to eq potepan_category_path(leaves_taxon.id)
      end
    end

    it "display product name" do
      expect(page).to have_content taxon.products.first.name
    end

    it "display product price" do
      expect(page).to have_content taxon.products.first.display_price
    end

    it "product link should be correct" do
      all('div.productBox')[0].click_link 'products-img'
      expect(current_path).to eq potepan_product_path(product_1.id)
    end

    context "when product image is not exist" do
      it "doesn't display image" do
        expect(page.all('div.productBox')[1]).not_to have_selector 'img'
      end
    end

    context "when product image is exist" do
      it "display image" do
        expect(page.all('div.productBox')[0]).to have_selector 'img'
      end
    end
  end
end
