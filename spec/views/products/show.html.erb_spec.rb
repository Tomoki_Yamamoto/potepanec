require 'rails_helper'

RSpec.describe "products/show.html.erb", type: :feature do
  let(:image) { create(:image) }
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }
  let!(:product_with_no_taxon) { create(:product) }

  before do
    product.images << image
    visit potepan_product_path(product.id)
  end

  describe "product-page" do
    it "should have the title 'product.name - BIGBAG Store'" do
      expect(page).to have_title("#{product.name} - BIGBAG Store")
    end

    it "display product name" do
      expect(page).to have_selector 'h2', text: product.name
    end

    it "display product name" do
      expect(page).to have_selector 'li.active', text: product.name
    end

    it "display product price" do
      expect(page).to have_selector 'h3', text: product.price
    end

    it "display product description" do
      expect(page).to have_selector 'p', text: product.description
    end

    it "display product images" do
      expect(product.images).to exist
    end

    context "when product has no taxon" do
      before do
        visit potepan_product_path(product_with_no_taxon.id)
      end

      it "'Homeへ戻る' should have Home link" do
        click_link 'Homeへ戻る'
        expect(current_path).to eq potepan_path
      end
    end

    context "when product has taxon" do
      it "'一覧ページへ戻る' should have correct taxon's link" do
        click_link '一覧ページへ戻る'
        expect(current_path).to eq potepan_category_path(product.taxons.first.id)
      end
    end

    describe "related_products" do
      before do
        related_products.each do |related_product|
          related_product.images << create(:image)
        end
      end

      it "display related_product name" do
        expect(page).to have_selector '.productCaption h5', text: related_products.first.name
      end

      it "display related_product price" do
        expect(page).to have_selector '.productCaption h3', text: related_products.first.price
      end
      it "should have correct related product link" do
        click_on related_products.first.name
        expect(current_path).to eq potepan_product_path(related_products.first.id)
      end

      it "display related_product images" do
        expect(related_products.first.images).to exist
      end

      it "display up to four related_products" do
        expect(page.all('div.productBox').size).to eq(4)
      end
    end
  end
end
