class Potepan::Suggest < ApplicationRecord
  scope :search_keyword, -> (keyword) { where("keyword LIKE ?", "#{keyword}%") }
end
