class Potepan::Api::V1::SuggestsController < Potepan::Api::BaseController
  before_action :authenticate
  rescue_from ActionController::BadRequest, with: :rescue400
  DEFAULT_SUGGEST_COUNT = 10
  MAX_SUGGEST_COUNT = 100
  MIN_SUGGEST_RANGE = 1
  MAX_SUGGEST_RANGE = 100

  def suggests
    if params[:keyword].present?
      suggest_count = amount_of_suggestion(params[:max_num].to_i)
      suggests = Potepan::Suggest.search_keyword(params[:keyword]).limit(suggest_count).pluck(:keyword)
      render json: suggests
    else
      render json: "keyword can't be blank", status: 400
    end
  end

  private

  def rescue400(e)
    render json: e.message, status: 400
  end

  def amount_of_suggestion(max_num)
    case
    when max_num >= MIN_SUGGEST_RANGE && max_num <= MAX_SUGGEST_RANGE
      max_num
    when max_num < MIN_SUGGEST_RANGE
      DEFAULT_SUGGEST_COUNT
    when max_num > MAX_SUGGEST_RANGE
      MAX_SUGGEST_COUNT
    else
      DEFAULT_SUGGEST_COUNT
    end
  end
end
