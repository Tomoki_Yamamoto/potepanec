class Potepan::Api::BaseController < ApplicationController
  include ActionController::HttpAuthentication::Token::ControllerMethods

  protected

  def authenticate
    authenticate_token || render_unauthorized
  end

  def authenticate_token
    authenticate_with_http_token do |token, options|
      api_key = Rails.application.credentials.suggest_api_key
      ActiveSupport::SecurityUtils.secure_compare(token, api_key)
    end
  end

  def render_unauthorized
    render json: "unauthorized", status: 401
  end
end
