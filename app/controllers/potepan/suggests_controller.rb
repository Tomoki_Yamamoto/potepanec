require 'potepan_api_suggest'

class Potepan::SuggestsController < ApplicationController
  MAX_SUGGEST_COUNT = 5

  def search
    suggest = PotepanApiSuggest.suggest(params[:keyword], MAX_SUGGEST_COUNT)
    if suggest.status == 200
      render json: JSON.parse(suggest.body)
    else
      render json: [], status: suggest.status
    end
  end
end
