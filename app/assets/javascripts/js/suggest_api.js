$(document).ready(function() {
  $("#keyword").autocomplete({
    source: function(req, res) {
      $.ajax({
        url: "potepan/suggest",
        datatype: "json",
        cache: false,
        data: { keyword: req.term },
        type: "GET"
      })
      .then(
        data => res(data),
        error => ['']
      );
    }
  });
});

$(document).ready(function() {
  $(".ui-front").hover(function() {
    $(".dropdown").addClass("open");
  });
});
