require 'httpclient'
require 'json'

class PotepanApiSuggest
  POTEPAN_API_KEY = Rails.application.credentials.potepan_api_key
  POTEPAN_URL = Rails.application.credentials.potepan_url

  def self.suggest(keyword, max_num)
    header = { Authorization: "Bearer #{POTEPAN_API_KEY}" }
    query = { keyword: keyword, max_num: max_num }
    client = HTTPClient.new
    client.get(POTEPAN_URL, header: header, query: query)
  end
end
